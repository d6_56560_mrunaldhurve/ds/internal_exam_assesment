import java.util.Scanner;

class Vehicle {
    private String company;
    private String model;
    private String type;
    private int price;
    private int no;
    private String colour;
    public Vehicle(int no) {
        this.no = no;
    }
    public Vehicle(String company, String model, String type, int price, int no, String colour) {
        this.company = company;
        this.model = model;
        this.type = type;
        this.price = price;
        this.no = no;
        this.colour = colour;   
  } 
    public Vehicle() {  }
    public int getNo() {
        return no;
    }
    public int getPrice() {
        return price;
    }
    @Override
    public String toString() {
        return "Vehicle [colour=" + colour + ", company=" + company + ", model=" + model + ", no=" + no + ", price="
                + price + ", type=" + type + "]";
    }  
  

}
class LinkedList{
    static  class Node {
         private Vehicle vehicle;
         private Node prev;//contains actual data of type int
         private Node next;//contains reference to the next node
 
         public Node(Vehicle vehicle ){
             this.vehicle = vehicle;
             this.next = null;
             this.prev=null;
         }
 
     }//end of Node class
 
     private Node head;//4 bytes
     private int vehiclesCount;
 
     public LinkedList(){
         head = null;
         vehiclesCount=0;
     }
     public int getVehiclesCount() {
        return vehiclesCount;
    }

     public  boolean isEmpty(){
        return head==null;
    }

     //1.Add At Last Node
     public  void addAtLast(Vehicle vehicle){
        Node newNode=new Node(vehicle);
         if (isEmpty()) {
             head=newNode;
             newNode.next=head;
             newNode.prev=newNode;
             vehiclesCount++;  
             System.out.println("Vehicle added successfully in empty ");  
         }else{
             newNode.next=head;
             newNode.prev=head.prev;
             head.prev.next=newNode;
             head.prev=newNode;
             vehiclesCount++; 
             System.out.println("Vehicle added successfully  ");  
         }
        }

        //2.Add At First Node
       public  void addAtFirst(Vehicle vehicle){
        Node newNode=new Node(vehicle);
        if (isEmpty()) {
            head=newNode;
            newNode.next=head;
            newNode.prev=newNode;
            vehiclesCount++;    
        }else{
            newNode.next=head;
            newNode.prev=head.prev;
           head.prev.next=newNode;
           head.prev=newNode;
           head=newNode;
            vehiclesCount++;    
        }
        System.out.println("Vehicle added successfully");
       }

       //3.Delete At Last
    public void DeleteAtLast(){
        if(isEmpty()){
            System.out.println("list is empty");
        }else{
            if(head.next==head){
                head=null;
                vehiclesCount--;
            }else{
                head.prev.prev.next=head;
                head.prev=head.prev.prev;
                vehiclesCount--;
            }
        }
    }
     //4.Delete At First
     public void DeleteAtFirst(){
        if(isEmpty()){
            System.out.println("list is empty");
        }else{
            if(head.next==head){
                head=null;
                vehiclesCount--;
            }else{
             head.prev.next=head.next;
             head.next.prev=head.prev;
             head=head.next;
             vehiclesCount--;
            }
        }
    }
    //5. Search By Number
    public void linearSearch(int data){
        Node trav=head;
        if(!isEmpty()){
         do {
           
             if(trav.vehicle.getNo()==data){
              System.out.println("Vehicle  is found =>>"+trav.vehicle);
             }
             else{
                System.out.println("key is not Matched");
             }
             trav=trav.next;   
         } while (trav!=head);
        }
        else{
            System.out.println("key is not Matched");
        }
            }

             //6.Display All Vehicles
      public void  displayVehicle(){
        Node trav=head;
     if(!isEmpty()){
      System.out.println("Number of Vehicles in list are "+getVehiclesCount());
      System.out.println("List in Forward direction is");
      do {
          System.out.println(trav.vehicle);
          trav=trav.next;   
      } while (trav!=head);
     }
     else{
         System.out.println("List is Empty");
     }
    }
     //7.displayReverseOrder
     public void  displayReverseOrder(){
        Node trav=head.prev;
     if(!isEmpty()){
      System.out.println("Number of Vehicles in list are "+getVehiclesCount());
      System.out.println("List in Reverse direction is");
      do {
          System.out.println(trav.vehicle);
          trav=trav.prev;   
      } while (trav!=head.prev);
     }
     else{
         System.out.println("List is Empty");
     }
    }
    //8.Sorting by price
    public  void  sortingByPrice()
    { 
        Node i,j=null;
        i=head;
        Vehicle temp;
        do {
            for(j=i.next;j!=head;j=j.next){
                temp=i.vehicle;
                if(i.vehicle.getPrice()>j.vehicle.getPrice())
                {
                    temp=i.vehicle;
                    i.vehicle=j.vehicle;
                    j.vehicle=temp;
                }
            }
            i=i.next;
       } while (i != head);
    }
 }
 
public class D6_Mrunal_56560{
    public static int menu(){
        Scanner sc=new Scanner(System.in);
        System.out.println("******* VEHICLE MENU ********");
        System.out.println("0. Exit ");
        System.out.println("1. add Last Vehicle object in to the list");
        System.out.println("2. add First Vehicle object in to the list");
        System.out.println("3. delete Last Vehicle object from the list");
        System.out.println("4. delete First Vehicle object from the list ");
        System.out.println("5. search Vehicle by No ");
        System.out.println("6. print all Vehicle details");
        System.out.println("7. print Vehicle list in reverse order");
        System.out.println("8. Sort Vehicle List by price ");
        System.out.println("Enter Your choice");
        int choice=sc.nextInt();
        return choice;
        
    }

    public static void main(String[] args) {
        LinkedList linkedlist=new LinkedList();
        Vehicle vehicle;
      Scanner sc=new Scanner(System.in);
   while (true) {
       int choice=menu();
       switch(choice){
           case 0:
           System.exit(0);
           break;

           case 1:
           System.out.println("Enter Vehicles Details ");
           System.out.println("Enter company name ");
           String company=sc.next();
           System.out.println("Enter model name ");
           String model=sc.next();
           System.out.println("Enter type ");
           String type=sc.next();
           System.out.println("Enter price ");
           int price=sc.nextInt();
           System.out.println("Enter number ");
           int no=sc.nextInt();
           System.out.println("Enter colour ");
           String colour=sc.next();
           vehicle =new Vehicle(company,model,type,price,no,colour);
           linkedlist.addAtLast(vehicle);


           case 2:
           System.out.println("Enter Vehicles Details ");
           System.out.println("Enter company name ");
           String company1=sc.next();
           System.out.println("Enter model name ");
           String model1=sc.next();
           System.out.println("Enter type ");
           String type1=sc.next();
           System.out.println("Enter price ");
           int price1=sc.nextInt();
           System.out.println("Enter number ");
           int no1=sc.nextInt();
           System.out.println("Enter colour ");
           String colour1=sc.next();
           vehicle =new Vehicle(company1,model1,type1,price1,no1,colour1);
           linkedlist.addAtFirst(vehicle);
           break;

           case 3:
            linkedlist.DeleteAtLast();
            System.out.println("Delete at Last Position Successfully");
            break;

           case 4:
            linkedlist.DeleteAtFirst();
            System.out.println("Delete at First Position Successfully");
            break;
           case 5:
            System.out.print("enter the vehicle Number :");
            int  data=sc.nextInt();
    
            linkedlist.linearSearch(data);
            break;
            case 6:
            linkedlist.displayVehicle();
            break;

            case 7:
            linkedlist. displayReverseOrder();
            System.out.println("LinkedList Reversed Successfully");
                        
            break;
            case 8:
            linkedlist.sortingByPrice();
            System.out.println("successfully sorted by Price");
            break;
           default:
           break;

       }
    }
}
}






